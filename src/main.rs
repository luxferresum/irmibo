use async_std::prelude::*;
use tide_websockets::{WebSocket, WebSocketConnection, Message};
use ratman_client::{RatmanIpc};

#[async_std::main]
async fn main() -> Result<(), std::io::Error> {
    env_logger::init();
    let mut app = tide::new();
    // app.at("/as_middleware")
    //     .with(WebSocket::new(|_request, mut stream| async move {
    //         while let Some(Ok(Message::Text(input))) = stream.next().await {
    //             let output: String = input.chars().rev().collect();

    //             stream
    //                 .send_string(format!("{} | {}", &input, &output))
    //                 .await?;
    //         }

    //         Ok(())
    //     }))
    //     .get(|_| async move { Ok("this was not a websocket request") });

    app.at("/ws")
        .get(WebSocket::new(|_request, mut stream| async move {
            let r = handle_connection(stream.clone()).await;
            match r {
                Ok(()) => {
                    println!("everything ok, closing connection");
                },
                Err(err) => {
                    println!("got an err, closing the connection {:?}", err);

                    stream
                        .send_string(format!("did flood!"))
                        .await
                        .unwrap()
                }
            }
            // let match handle_connection(stream).async {
            //     Ok() -> {},
            //     Err(_err) -> {},
            // };
            // println!("get /ws connect request");
            // let mut ratman: Option<RatmanIpc> = None;

            // if let Ok(ipc) = RatmanIpc::default().await {
            //     ratman = Some(ipc);
            // }

            // async_std::task::spawn(async {
            //     while let Some(Ok(Message::Text(input))) = stream.next().await {
            //         let output: String = input.chars().rev().collect();
            //         if let Some(ipc) = &ratman {
            //             ipc.flood(vec![1,2,3]).await.unwrap();
            //             stream
            //                 .send_string(format!("did flood!"))
            //                 .await
            //                 .unwrap();
            //         }
            //     }
            // });

            Ok(())
        }));
    
    // app.at("/connect/:session")
    //     .get

    app.listen("127.0.0.1:8080").await?;

    Ok(())
}

async fn handle_connection(mut stream: WebSocketConnection) -> ratman_client::Result<()> {
    // let  Some(Ok(Message::Text(input))) = 
    let first_msg = stream.next().await.unwrap().unwrap();
    let first_str = first_msg.to_text().unwrap();
    println!("got first message: {}", first_str);

    let ratman = if first_str.eq("new") {
        RatmanIpc::default().await.unwrap()
    } else {
        RatmanIpc::connect(
            "127.0.0.1:9020",
            Some(ratman_client::Identity::from_string(&String::from(first_str)))
        ).await.unwrap()
    };

    stream
        .send_string(format!("{}", ratman.address().to_string()))
        .await
        .unwrap();
    
    let frontend_to_backend_handle = async_std::task::spawn(
        frontend_to_backend(stream.clone(), ratman.clone())
    );

    let backend_to_frontend_handle = async_std::task::spawn(
        backend_to_frontend(stream.clone(), ratman.clone())
    );

    frontend_to_backend_handle.race(backend_to_frontend_handle).await;

    Ok(())
}

async fn frontend_to_backend(mut stream: WebSocketConnection, ratman: RatmanIpc) {
    while let Some(Ok(Message::Text(input))) = stream.next().await {
        println!("flood: {}", input);
        let data = input.as_bytes().to_vec();
        let res = ratman.flood(data).await;
        if let Err (_err) = res {
            return;
        }
    }
}

async fn backend_to_frontend(stream: WebSocketConnection, ratman: RatmanIpc) {
    println!("waiting for messages from ratman");
    while let Some((_, msg)) = ratman.next().await {
        let x = msg.payload;
        let msg_str = String::from_utf8(x).unwrap();
        println!("got a message from ratman: {:?}", msg_str);
        stream
            .send_string(msg_str)
            .await
            .unwrap();
    }
    println!("done waiting for messages from ratman");
}
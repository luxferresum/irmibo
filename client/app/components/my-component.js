import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

export default class MyComponent extends Component {
  @tracked addr;
  @tracked input = 'SQUEEK';

  constructor() {
    super(...arguments);
    this.socket = new WebSocket('ws://localhost:8080/ws');
    this.socket.onopen = () => {
      const saved_addr = localStorage.getItem('addr');
      if (saved_addr) {
        this.socket.send(saved_addr);
      } else {
        this.socket.send('new');
      }
      // setInterval(() => {
      //   this.socket.send('bla')
      // }, 1000);
    };
    this.socket.onmessage = (event) => {
      if (!this.addr) {
        this.addr = event.data;
        localStorage.setItem('addr', this.addr);
        // this.socket.send('hello I am now online');
        console.log('connection to ratman established');
      } else {
        console.log('got squeek', event.data);
      }
    };
  }

  @action
  squeek() {
    this.socket.send(this.input);
  }
}
